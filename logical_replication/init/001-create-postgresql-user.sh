set -e
createuser -s postgres -U user
psql -U postgres -c "CREATE USER replicarole WITH REPLICATION ENCRYPTED PASSWORD 'SuperSecret'" postgres
psql -U postgres -c "alter role postgres password 'SuperSecret'" postgres
# Prima macchina master
if [ "$SERVER_NAME" == "pg_3_dev1" ]; then
  { echo "host    replication     replicarole      0.0.0.0/0      trust"; } |  tee -a "$PGDATA/pg_hba.conf" > /dev/null
  #wal level to logical
  psql -U postgres -c "ALTER SYSTEM set wal_level = logical;" postgres
  pg_ctl -U postgres -D "$PGDATA" -m fast -w stop
  pg_ctl -U postgres -D "$PGDATA" start
  #Create a demo db
  psql -U postgres -c "create database meetup;" postgres
  psql -U postgres -c "create table itpug_event (id serial not null primary key,description varchar(100));" meetup
  psql -U postgres -c "insert into itpug_event (description) values ('Bari meetup ... have a lot of fun !!');" meetup
  # Grant select permission di replicarole user
  psql -U postgres -c "grant SELECT on ALL tables in schema public to replicarole ;" meetup
  # Create  publication
  psql -U postgres -c "create publication itpug_pub for table itpug_event;" meetup	
fi
#Seconda macchina slave
if [ "$SERVER_NAME" == "pg_3_dev2" ]; then
  sleep 10
  #Create a demo db
  psql -U postgres -c "create database meetup;" postgres
  psql -U postgres -c "create table itpug_event (id serial not null primary key,description varchar(100));" meetup
  # Create subscription
  psql -U postgres -c "create subscription itpug_sub connection 'user=replicarole password=SuperSecret host=pg_3_dev1 port=5432 dbname=meetup' publication itpug_pub;" meetup
fi
