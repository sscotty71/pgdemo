set -e
createuser -s postgres -U user
psql -U postgres -c "CREATE USER replicarole WITH REPLICATION ENCRYPTED PASSWORD 'SuperSecret'" postgres
psql -U postgres -c "alter role postgres password 'SuperSecret'" postgres
# Prima macchina master
if [ "$SERVER_NAME" == "pg_2_dev1" ]; then
  { echo "host    replication     replicarole      0.0.0.0/0      trust"; } |  tee -a "$PGDATA/pg_hba.conf" > /dev/null
  psql -U postgres -c "select pg_reload_conf();SELECT * FROM pg_create_physical_replication_slot('pg_replica1');" postgres
fi
#Seconda macchina slave
if [ "$SERVER_NAME" == "pg_2_dev2" ]; then
  sleep 10
  pg_ctl -U postgres -D "$PGDATA" -m fast -w stop
  rm -rf $PGDATA/*
  pg_basebackup -h pg_2_dev1 -U replicarole -p 5432 -D $PGDATA -Fp -Xs -P -R -W -S pg_replica1
  chown -R postgres.postgres $PGDATA
  pg_ctl -U postgres -D "$PGDATA" start
  { echo "host    replication     replicarole      0.0.0.0/0      trust"; } |  tee -a "$PGDATA/pg_hba.conf" > /dev/null
  psql -U postgres -c "select pg_reload_conf();SELECT * FROM pg_create_physical_replication_slot('pg_replica2');" postgres
fi
#Terza macchina slave
if [ "$SERVER_NAME" == "pg_2_dev3" ]; then
  sleep 20
  pg_ctl -U postgres -D "$PGDATA" -m fast -w stop
  rm -rf $PGDATA/*
  pg_basebackup -h pg_2_dev2 -U replicarole -p 5432 -D $PGDATA -Fp -Xs -P -R -W -S pg_replica2
  chown -R postgres.postgres $PGDATA
  pg_ctl -U postgres -D "$PGDATA" start
fi
