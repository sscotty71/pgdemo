set -e
createuser -s postgres -U user
psql -U postgres -c "CREATE USER replicarole WITH REPLICATION ENCRYPTED PASSWORD 'SuperSecret'" postgres
psql -U postgres -c "alter role postgres password 'SuperSecret'" postgres
# Prima macchina master
if [ "$SERVER_NAME" == "pg_5_dev1" ]; then
  { echo "host    replication     replicarole      0.0.0.0/0      trust"; } |  tee -a "$PGDATA/pg_hba.conf" > /dev/null
  psql -U postgres -c "select pg_reload_conf();SELECT * FROM pg_create_physical_replication_slot('pg_replica1');" postgres
  psql -U postgres -c "alter system set synchronous_standby_names='replica1';" postgres
  psql -U postgres -c "alter system set synchronous_commit = 'on';" postgres
  pg_ctl -U postgres -D "$PGDATA" -m fast -w stop
  pg_ctl -U postgres -D "$PGDATA" start
fi
#Seconda macchina slave
if [ "$SERVER_NAME" == "pg_5_dev2" ]; then
  sleep 10
  pg_ctl -U postgres -D "$PGDATA" -m fast -w stop
  rm -rf $PGDATA/*
  pg_basebackup -h pg_5_dev1 -U replicarole -p 5432 -D $PGDATA -Fp -Xs -P -R -W -S pg_replica1
  chown -R postgres.postgres $PGDATA
  pg_ctl -U postgres -D "$PGDATA" start
  psql -U postgres -c "alter system set primary_conninfo = 'user=replicarole password=SuperSecret channel_binding=prefer host=pg_5_dev1 port=5432 sslmode=prefer sslcompression=0 ssl_min_protocol_version=TLSv1.2 gssencmode=prefer krbsrvname=postgres target_session_attrs=any application_name=replica1';" postgres
  pg_ctl -U postgres -D "$PGDATA" -m fast -w stop
  pg_ctl -U postgres -D "$PGDATA" start
fi
